﻿using JetBrains.Annotations;
using UnityEngine;

namespace Assets.Scripts.TestCases
{
    public interface IComparableTerm
    {
        bool Compare(GameObject go);
    }
}