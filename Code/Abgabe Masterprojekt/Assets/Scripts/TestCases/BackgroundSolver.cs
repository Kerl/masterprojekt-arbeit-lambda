﻿using System.Collections.Generic;
using Assets.Scripts.Helper;
using Assets.Scripts.TestCases;
using UnityEngine;

namespace Assets.Scripts.Utility
{
    public class BackgroundSolver : MonoBehaviour {

        // Use this for initialization
        void Start () {
	
        }
	
        // Update is called once per frame
        void Update ()
        {
            if (Input.GetKeyDown(KeyCode.O))
            {
                RunTestCase("01");
                RunTestCase("02");
            }
        }

        public bool RunTestCases()
        {
            for (int i = 0; i < GameObject.Find("Utility").transform.Find("Testing").Find("TestCases").childCount; i++)
            {
                var testCase = GameObject.Find("Utility").transform.Find("Testing").Find("TestCases").GetChild(i);
                if (!RunTestCase(testCase.name))
                {
                    return false;
                }
            }

            //if (RunTestCase("01") == true && RunTestCase("02") == true)
            //{
            //    return true;
            //}

            return true;
        }

        private bool RunTestCase(string testCase)
        {
            var testCaseObject = GameObject.Find("Utility").transform.Find("Testing").Find("TestCases").Find(testCase);
            var testSubject = GameObject.Find("Utility").transform.Find("Testing").Find("TestSubject").GetChild(0);

            var input = testCaseObject.transform.Find("Input").GetChild(0);
            var testCaseSpawn = FindTestCaseSpawn(testSubject.gameObject, input.GetComponent<TestCaseParameter>().Id);
            var spawn = testCaseSpawn.GetComponent<SpawnBehavior>();

            spawn.GetNewOb(input.gameObject, 4, 0.314f);

            var level = TermManager.FindRootTerm();
            level.transform.SetParent(GameObject.Find("Utility").transform);
            testSubject.SetParent(GameObject.Find("Level").transform);
            TermManager.UpdateTerm();

            var testResultObject = SolveInBackground(testCase, testSubject.gameObject);

            level.transform.SetParent(GameObject.Find("Level").transform);
            testSubject.SetParent(GameObject.Find("Utility").transform.Find("Testing").Find("TestSubject"));


            var expectedOutput = testCaseObject.Find("Output").GetChild(0);
            var testReslt = expectedOutput.GetComponent<IComparableTerm>().Compare(testResultObject.gameObject);

            Destroy(GameObject.Find("Utility").transform.Find("BackgroundSolving").Find(testCase).gameObject);

            Debug.Log("Test result for test case " + testCase + ": " + testReslt);

            return testReslt;
        }

        public GameObject SolveInBackground(string testCase, GameObject ob)
        {
            var testCaseWrapper = new GameObject();
            testCaseWrapper.name = testCase;
            testCaseWrapper.transform.SetParent(GameObject.Find("Utility").transform.Find("BackgroundSolving"));

            var term = Instantiate(ob);
            term.transform.SetParent(testCaseWrapper.transform);
            term.SetActive(false);
            TermManager.StripVisuals(term);

            var nextEval = TermManager.FindNextEval(term);

            while (nextEval != null)
            {
            var boundVariables = nextEval.GetComponent<ApplicationBehavior>().GetRightTerm().GetComponent<AbstractionBehavior>().BoundVariables;
            var tempList = new List<GameObject>(boundVariables);

            foreach (var boundVariable in tempList)
            {
                boundVariable.GetComponent<TermBehavior>().CurrentSpawn.GetNewOb(nextEval.GetComponent<ApplicationBehavior>().GetLeftTerm(), 4, 1);
            }

            var abstraction = nextEval.GetComponent<ApplicationBehavior>().GetRightTerm();

            if (abstraction.GetComponent<ICalculatingAbstraction>() == null)
            {
                var spawn = abstraction.transform.Find("Visuals").Find("LeftSpawn").GetComponent<SpawnBehavior>();
                var currentGameObject = spawn.CurrentGameObject;
                currentGameObject.transform.position = abstraction.transform.parent.position;
                if (abstraction.transform.parent.parent != null)
                {
                    currentGameObject.transform.SetParent(abstraction.transform.parent.parent);
                }
                currentGameObject.transform.localScale = abstraction.transform.parent.localScale;

                var p = abstraction.transform.parent.parent;
                if (p != null && p.name != testCase)
                {
                    var targetSpawn = abstraction.transform.parent.GetComponent<TermBehavior>().CurrentSpawn;
                    targetSpawn.CurrentGameObject = currentGameObject;
                    targetSpawn.CurrentColorIndex =
                        abstraction.transform.parent.Find("Visuals").Find("RightSpawn").GetComponent<SpawnBehavior>().CurrentColorIndex;
                }
            }
            else
            {
                var spawn = abstraction.transform.Find("Visuals").Find("LeftSpawn").GetComponent<SpawnBehavior>();
                var currentGameObject = abstraction.gameObject.GetComponent<ICalculatingAbstraction>().CalculateOutput(nextEval.GetComponent<ApplicationBehavior>().GetLeftTerm());
                currentGameObject.transform.position = abstraction.transform.parent.position;
                if (abstraction.transform.parent.parent != null)
                {
                    currentGameObject.transform.SetParent(abstraction.transform.parent.parent);
                }
                currentGameObject.transform.localScale = abstraction.transform.parent.localScale;

                var p = abstraction.transform.parent.parent;
                if (p != null && p.name != testCase)
                {
                    var targetSpawn = abstraction.transform.parent.GetComponent<TermBehavior>().CurrentSpawn;
                    targetSpawn.CurrentGameObject = currentGameObject;
                    targetSpawn.CurrentColorIndex =
                        abstraction.transform.parent.Find("Visuals").Find("RightSpawn").GetComponent<SpawnBehavior>().CurrentColorIndex;
                }
            }

            abstraction.transform.parent.SetParent(null);
            Destroy(abstraction.transform.parent.gameObject);
            nextEval.GetComponent<ApplicationBehavior>().GetLeftTerm().transform.parent = null;
            Destroy(nextEval.GetComponent<ApplicationBehavior>().GetLeftTerm());

            term = testCaseWrapper.transform.GetChild(0).gameObject;
            nextEval = TermManager.FindNextEval(term);
            }

            TermManager.StripVisuals(term);
            term.SetActive(false);

            TermManager.PerformCleanup();

            return term;
        }

        private TestCaseSpawn FindTestCaseSpawn(GameObject ob, int id)
        {
            if (ob.GetComponent<TestCaseSpawn>() != null && ob.GetComponent<TestCaseSpawn>().Id == id)
            {
                return ob.GetComponent<TestCaseSpawn>();
            }
            else
            {
                foreach (Transform child in ob.transform)
                {
                    var s = FindTestCaseSpawn(child.gameObject, id);

                    if (s != null)
                    {
                        return s;
                    }
                }
            }

            return null;
        }
    }
}
