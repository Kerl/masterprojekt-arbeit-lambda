﻿using Assets.Scripts.Helper;
using UnityEngine;

namespace Assets.Scripts.TestCases
{
    public class AdditionalTestCaseCheck01 : MonoBehaviour, IAdditionalTestCaseCheck
    {
        public bool Check()
        {
            if (TermManager.FindRootTerm().GetComponent<BooleanBehavior>() != null)
            {
                return true;
            }

            return false;
        }
    }
}
