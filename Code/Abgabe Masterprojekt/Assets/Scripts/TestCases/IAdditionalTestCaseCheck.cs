﻿namespace Assets.Scripts.TestCases
{
    public interface IAdditionalTestCaseCheck
    {
        bool Check();
    }
}