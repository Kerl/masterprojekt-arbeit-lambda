﻿using Assets.Scripts.Helper;
using Assets.Scripts.Utility;
using UnityEngine;

namespace Assets.Scripts
{
    public class ConveyorBehavior : MonoBehaviour
    {
        private GameObject _leftTerm, _rightTerm;
        private bool _isActive;

        // Use this for initialization
        void Start () {
	
        }
	
        // Update is called once per frame
        void Update ()
        {
            if (_leftTerm == null)
            {
                _isActive = false;
            }

            if (_isActive)
            {
                _leftTerm.transform.position += Vector3.right * 4 * _leftTerm.transform.lossyScale.x * Time.deltaTime;
            }
        }

        public void StartSolving()
        {
            _leftTerm = transform.parent.Find("LeftSpawn").GetComponent<SpawnBehavior>().CurrentGameObject;
            _rightTerm = transform.parent.Find("RightSpawn").GetComponent<SpawnBehavior>().CurrentGameObject;

            if (_leftTerm != null && _rightTerm != null &&
                _rightTerm.GetComponent<TermBehavior>().TermType == TermTypes.Abstraction)
            {
                _rightTerm.AddComponent<AbstractionSolvingBehavior>();
                _rightTerm.transform.GetComponent<BoxCollider2D>().isTrigger = true;

                LayerManager.ApplyLayerOffset(_leftTerm.transform, -2);
                var rb = _leftTerm.AddComponent<Rigidbody2D>();
                rb.constraints = RigidbodyConstraints2D.FreezeAll;

                _isActive = true;
            }
        }
    }
}
