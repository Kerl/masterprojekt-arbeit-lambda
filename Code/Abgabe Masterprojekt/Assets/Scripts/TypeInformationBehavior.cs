﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Helper;
using Assets.Scripts.TypeSystem;
using Assets.Scripts.Utility;
using UnityEngine;

namespace Assets.Scripts
{
    public class TypeInformationBehavior : MonoBehaviour
    {
        public LambdaTypes LambdaType;

        public TypeBase GetLambdaType()
        {
            if (LambdaType == LambdaTypes.Boolean)
            {
                return new TypeBoolean();
            }
            else if (LambdaType == LambdaTypes.Number)
            {
                return new TypeNumber();
            }
            else if (LambdaType == LambdaTypes.Function)
            {
                return new TypeFunction();
            }

            return null;
        }
    }
}
