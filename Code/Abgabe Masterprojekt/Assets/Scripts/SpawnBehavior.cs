﻿using System.Runtime.CompilerServices;
using Assets.Scripts.Helper;
using Assets.Scripts.Utility;
using UnityEngine;

namespace Assets.Scripts
{
    public class SpawnBehavior : MonoBehaviour
    {
        public int CurrentColorIndex = -1;
        public GameObject CurrentGameObject;

        private static readonly Color ApplicationColorOne = new Color(223f/255f, 222f/255f, 169f/255f);
        private static readonly Color ApplicationColorTwo = new Color(223f/255f, 169f/255f, 169f/255f);
        private GameObject _oldObject;
        private TermStore _termStore;

        // Use this for initialization
        void Start()
        {
            _termStore = GameObject.Find("Utility").GetComponent<TermStore>();
        }

        // Update is called once per frame
        void Update()
        {
        }

        public void CopyToClipboard()
        {
            if (CurrentGameObject != null)
            {
                var clipboardTerm = Instantiate(CurrentGameObject);
                clipboardTerm.transform.SetParent(_termStore.gameObject.transform);
                clipboardTerm.SetActive(false);

                _termStore.AddTerm(clipboardTerm, TermTypes.Clipboard);
            }
        }

        public GameObject GetNewOb(GameObject prefab, int layerOffset, float scale)
        {
            var newOb = (GameObject) Instantiate(prefab, transform.position, transform.rotation);

            if (newOb.name.Contains("abstraction"))
            {
                newOb.name = "Abstraction";
            }
            else if (newOb.name.Contains("application"))
            {
                newOb.name = "Application";
            }
            else if (newOb.name.Contains("variable"))
            {
                newOb.name = "Variable";
            }

            Destroy(newOb.GetComponent<Rigidbody2D>());
            newOb.transform.localScale = transform.parent.parent.lossyScale*scale;
            newOb.transform.SetParent(transform.parent.parent);
            newOb.GetComponent<TermBehavior>().CurrentSpawn = this;

            LayerManager.ApplyLayerOffset(newOb.transform,
                transform.parent.Find("Background").GetComponent<SpriteRenderer>().sortingOrder + layerOffset);

            newOb.SetActive(true);

            _oldObject = CurrentGameObject;
            CurrentGameObject = newOb;

            LayerManager.ApplyColor(newOb);
            PerformTransition();

            TermManager.UpdateTerm();

            return newOb;
        }

        private static void ApplyColor(GameObject ob)
        {
            var spriteRenderer = ob.transform.Find("Visuals").Find("Background").GetComponent<SpriteRenderer>();

            if (spriteRenderer.sortingOrder%8 == 0)
            {
                spriteRenderer.color = ApplicationColorOne;
            }
            else
            {
                spriteRenderer.color = ApplicationColorTwo;
            }
        }

        private void PerformTransition()
        {
            if (_oldObject != null)
            {
                _oldObject.transform.parent = null;
                StartCoroutine(Coroutines.CR_DoScale(_oldObject.transform.localScale, Vector3.zero, 0.1f, _oldObject,
                    new CompletionJobDelete()));
            }

            //if (CurrentGameObject != null)
            //{
            //    StartCoroutine(Coroutines.CR_DoScale(Vector3.zero, CurrentGameObject.transform.localScale, 0.1f,
            //        CurrentGameObject,
            //        null));
            //}
        }
    }
}