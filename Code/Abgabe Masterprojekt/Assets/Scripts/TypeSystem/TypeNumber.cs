﻿namespace Assets.Scripts.TypeSystem
{
    class TypeNumber: TypeBase
    {
        public TypeNumber()
        {
            LambdaTypes = LambdaTypes.Number;
        }
    }
}
