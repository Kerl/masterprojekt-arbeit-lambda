﻿namespace Assets.Scripts.TypeSystem
{
    class TypeFunction: TypeBase
    {
        public TypeBase LType, RType;

        public TypeFunction()
        {
            LambdaTypes = LambdaTypes.Function;
        }

        public TypeFunction(TypeBase lType, TypeBase rType)
        {
            LambdaTypes = LambdaTypes.Function;
            LType = lType;
            RType = rType;
        }
    }
}
