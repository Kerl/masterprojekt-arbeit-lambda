﻿using UnityEngine;

namespace Assets.Scripts.TypeSystem
{
    public class TypeBase
    {
        public LambdaTypes LambdaTypes;

        public static bool Compare(TypeBase type1, TypeBase type2)
        {
            if (type1.GetType() == type2.GetType())
            {
                if (type1.GetType() == typeof(TypeFunction))
                {
                    var func1 = (TypeFunction)type1;
                    var func2 = (TypeFunction)type2;

                    return true & Compare(func1.LType, func2.LType) & Compare(func1.RType, func2.RType);
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
