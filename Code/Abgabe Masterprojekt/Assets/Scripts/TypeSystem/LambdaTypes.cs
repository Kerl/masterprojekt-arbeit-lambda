﻿namespace Assets.Scripts.TypeSystem
{
    public enum LambdaTypes
    {
        Boolean,
        Number,
        Function
    }
}
