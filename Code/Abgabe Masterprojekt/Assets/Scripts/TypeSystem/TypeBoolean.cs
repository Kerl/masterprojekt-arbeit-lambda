﻿namespace Assets.Scripts.TypeSystem
{
    class TypeBoolean: TypeBase
    {
        public TypeBoolean()
        {
            LambdaTypes = LambdaTypes.Boolean;
        }
    }
}
