﻿namespace Assets.Scripts.TypeSystem
{
    public interface ITypedTerm
    {
        TypeBase GetLambdaType();
    }
}