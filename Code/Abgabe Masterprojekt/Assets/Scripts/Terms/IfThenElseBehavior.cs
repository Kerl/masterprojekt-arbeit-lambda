﻿using Assets.Scripts.TypeSystem;
using UnityEngine;

namespace Assets.Scripts
{
    public class IfThenElseBehavior : MonoBehaviour, ICalculatingAbstraction
    {

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public GameObject CalculateOutput(GameObject input)
        {
            var left = transform.Find("Application").Find("Visuals").Find("LeftSpawn").GetComponent<SpawnBehavior>().CurrentGameObject;
            var right = transform.Find("Application").Find("Visuals").Find("RightSpawn").GetComponent<SpawnBehavior>().CurrentGameObject;

            var boolIn = input.GetComponent<BooleanBehavior>();

            if (boolIn.CurrentValue)
            {
                return left;
            }
            else
            {
                return right;
            }
        }

        public TypeBase GetPredefinedTypeBody()
        {
            throw new System.NotImplementedException();
        }
    }
}
