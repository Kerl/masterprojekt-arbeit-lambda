﻿using Assets.Scripts.TypeSystem;
using UnityEngine;

namespace Assets.Scripts
{
    public class PlusBehavior : MonoBehaviour, ICalculatingAbstraction {

        // Use this for initialization
        void Start () {
	
        }
	
        // Update is called once per frame
        void Update () {
	
        }

        public GameObject CalculateOutput(GameObject input)
        {
            var left = transform.Find("Application").Find("Visuals").Find("LeftSpawn").GetComponent<SpawnBehavior>().CurrentGameObject;
            var right = transform.Find("Application").Find("Visuals").Find("RightSpawn").GetComponent<SpawnBehavior>().CurrentGameObject;

            var leftValue = left.GetComponent<NumberBehavior>().CurrentNumber;
            var rightValue = right.GetComponent<NumberBehavior>().CurrentNumber;

            left.GetComponent<NumberBehavior>().SetNumber(leftValue+rightValue);

            return left;
        }

        public TypeBase GetPredefinedTypeBody()
        {
            return new TypeNumber();
        }
    }
}
