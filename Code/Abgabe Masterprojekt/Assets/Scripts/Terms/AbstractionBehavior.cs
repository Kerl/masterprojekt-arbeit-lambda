﻿using System.Collections.Generic;
using Assets.Scripts.Helper;
using Assets.Scripts.Terms;
using Assets.Scripts.TypeSystem;
using Assets.Scripts.Utility;
using UnityEngine;

namespace Assets.Scripts
{
    public class AbstractionBehavior : TypedTermMonoBehavior, IFillabeTerm, ITypedTerm
    {
        public string PredefinedRequiredType;
        public List<GameObject> BoundVariables;
        public GameObject PrefabPlaceholder;

        // Use this for initialization
        void Start () {
            BoundVariables = new List<GameObject>();
        }
	
        // Update is called once per frame
        void Update () {
            if (Input.GetKeyDown(KeyCode.B))
            {
                Debug.Log(GetLambdaType());
            }
        }

        public int GetCurrentVariableId()
        {
            return gameObject.transform.Find("Visuals").Find("HeadSpawn").GetComponent<HeadBehavior>().CurrentVariableId;
        }

        public void StartHeadAnimation()
        {
            gameObject.transform.Find("Visuals").Find("PaintableSpawn").Find("Canvas").gameObject.SetActive(true);

            foreach (var variable in BoundVariables)
            {
                variable.transform.Find("UI").Find("Canvas").gameObject.SetActive(true);
            }
        }

        public bool HasHeadApplied()
        {
            if (transform.Find("Visuals").Find("HeadSpawn").GetComponent<HeadBehavior>().CurrentVariableId != -1)
            {
                return true;
            }

            return false;
        }

        public GameObject GetTerm()
        {
            return transform.Find("Visuals").Find("LeftSpawn").GetComponent<SpawnBehavior>().CurrentGameObject;
        }

        public SpawnBehavior GetSpawn()
        {
            return transform.Find("Visuals").Find("LeftSpawn").GetComponent<SpawnBehavior>();
        }

        public TypeInformationBehavior GetHeadTypeInformation()
        {
            var typeInfo = transform.Find("Visuals").Find("TypeSpawn").GetComponent<SpawnBehavior>().CurrentGameObject;

            if (typeInfo != null)
            {
                return typeInfo.GetComponent<TypeInformationBehavior>();
            }

            return null;
        }

        public bool CheckForFilledSlots()
        {
            return !(GetTerm() == null || GetTerm().GetComponent<TermBehavior>().TermType == TermTypes.Placeholder);
        }

        public void SpawnPlaceholders()
        {
            if (GetTerm() == null)
            {
                GetSpawn().GetNewOb(PrefabPlaceholder, 4, 0.314f);
            }
        }

        public string GetRequiredType()
        {
            if (PredefinedRequiredType != "")
            {
                return PredefinedRequiredType;
            }

            return "No predefined type!";
        }

        public TypeBase GetLambdaType()
        {
            var lambdaTypeHead = GetLambdaTypeHead();
            var lambdaTypeBody = GetLambdaTypeBody();

            return new TypeFunction(lambdaTypeHead, lambdaTypeBody);
        }

        public TypeBase GetLambdaTypeHead()
        {
            var typeInfo = GetHeadTypeInformation();
            var lambdaType = GetLambdaTypeHeadInternal(typeInfo);
            return lambdaType;
        }

        public TypeBase GetLambdaTypeBody()
        {
            if (GetComponent<ICalculatingAbstraction>() == null)
            {
                var lambdaType = GetTerm().GetComponent<ITypedTerm>().GetLambdaType();
                return lambdaType;
            }
            else
            {
                return GetComponent<ICalculatingAbstraction>().GetPredefinedTypeBody();
            }
        }

        private TypeBase GetLambdaTypeHeadInternal(TypeInformationBehavior typeInfo)
        {
            if (typeInfo == null)
            {
                return null;
            }

            var lambdaType = typeInfo.GetLambdaType();

            if (typeInfo.LambdaType == LambdaTypes.Boolean || typeInfo.LambdaType == LambdaTypes.Number)
            {
                return lambdaType;
            }
            else if (typeInfo.LambdaType == LambdaTypes.Function)
            {
                TypeFunction funcType = (TypeFunction)lambdaType;

                var lTypeInfo =
                    typeInfo.gameObject.GetComponent<ApplicationBehavior>()
                        .GetLeftTerm()
                        .GetComponent<TypeInformationBehavior>();

                funcType.LType = GetLambdaTypeHeadInternal(lTypeInfo);

                var rTypeInfo =
                    typeInfo.gameObject.GetComponent<ApplicationBehavior>()
                        .GetRightTerm()
                        .GetComponent<TypeInformationBehavior>();

                funcType.RType = GetLambdaTypeHeadInternal(rTypeInfo);

                return funcType;
            }

            return null;
        }
    }
}
