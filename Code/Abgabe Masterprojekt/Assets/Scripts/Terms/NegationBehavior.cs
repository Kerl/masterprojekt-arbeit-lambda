﻿using Assets.Scripts.TypeSystem;
using UnityEngine;

namespace Assets.Scripts
{
    public class NegationBehavior : MonoBehaviour, ICalculatingAbstraction {

        // Use this for initialization
        void Start () {
	
        }
	
        // Update is called once per frame
        void Update ()
        {

        }

        public GameObject CalculateOutput(GameObject input)
        {
            var go = transform.Find("Visuals").Find("LeftSpawn").GetComponent<SpawnBehavior>().CurrentGameObject;

            if (go.GetComponent<ITypedTerm>() != null && go.GetComponent<ITypedTerm>().GetLambdaType().GetType() == typeof(TypeBoolean))
            {
                var currentValue = go.GetComponent<BooleanBehavior>().CurrentValue;
                go.GetComponent<BooleanBehavior>().SetCurrentValue(!currentValue);
                return go;
            }

            return null;
        }

        public TypeBase GetPredefinedTypeBody()
        {
            return new TypeBoolean();
        }
    }
}
