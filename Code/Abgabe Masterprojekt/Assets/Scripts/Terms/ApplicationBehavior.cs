﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters;
using System.Text;
using Assets.Scripts.Helper;
using Assets.Scripts.Terms;
using Assets.Scripts.TypeSystem;
using Assets.Scripts.Utility;
using UnityEngine;

namespace Assets.Scripts
{
    public class ApplicationBehavior : TypedTermMonoBehavior, IFillabeTerm, ITypedTerm
    {
        public GameObject PrefabPlaceholder;

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.J))
            {
                Debug.Log(TypeBase.Compare(GetRightTerm().GetComponent<AbstractionBehavior>().GetLambdaTypeHead(), GetLeftTerm().GetComponent<ITypedTerm>().GetLambdaType()));
            }
        }

        public bool IsSolvable()
        {
            var left = GetLeftTerm();
            var right = GetRightTerm();

            if (right != null
                && right.GetComponent<TermBehavior>().TermType == TermTypes.Abstraction
                && left != null)
            {
                return true;
            }

            return false;
        }

        public GameObject GetRightTerm()
        {
            return transform.Find("Visuals").Find("RightSpawn").GetComponent<SpawnBehavior>().CurrentGameObject;
        }

        public GameObject GetLeftTerm()
        {
            return transform.Find("Visuals").Find("LeftSpawn").GetComponent<SpawnBehavior>().CurrentGameObject;
        }

        public SpawnBehavior GetRightSpawn()
        {
            return transform.Find("Visuals").Find("RightSpawn").GetComponent<SpawnBehavior>();
        }

        public SpawnBehavior GetLeftSpawn()
        {
            return transform.Find("Visuals").Find("LeftSpawn").GetComponent<SpawnBehavior>();
        }

        public void SetPulsing(bool shouldPulse)
        {
            transform.Find("Visuals").Find("Flash").gameObject.SetActive(shouldPulse);
        }

        public void StartSolving()
        {
            transform.Find("Visuals").Find("Conveyor").GetComponent<ConveyorBehavior>().StartSolving();
        }

        public bool CheckForFilledSlots()
        {
            return
                !((GetLeftTerm() == null || GetLeftTerm().GetComponent<TermBehavior>().TermType == TermTypes.Placeholder) ||
                  (GetRightTerm() == null || GetRightTerm().GetComponent<TermBehavior>().TermType == TermTypes.Placeholder));
        }

        public void SpawnPlaceholders()
        {
            if (GetLeftTerm() == null)
            {
                GetLeftSpawn().GetNewOb(PrefabPlaceholder, 4, 0.314f);
            }

            if (GetRightTerm() == null)
            {
                GetRightSpawn().GetNewOb(PrefabPlaceholder, 4, 0.314f);
            }
        }

        public void ShowTypeWarning()
        {
            transform.Find("Visuals").Find("TypeWarning").gameObject.SetActive(true);
        }

        public void HideTypeWarning()
        {
            transform.Find("Visuals").Find("TypeWarning").gameObject.SetActive(false);
        }

        protected override string GetReturnTypeInternal()
        {
            if (GetRightTerm() != null)
            {
                var abstraction = GetRightTerm().GetComponent<AbstractionBehavior>();
                var application = GetRightTerm().GetComponent<ApplicationBehavior>();

                if (abstraction != null)
                {
                    return abstraction.GetReturnType();
                }

                if (application != null)
                {
                    return application.GetReturnType().Substring(1);
                }
            }

            return "Application will not be solved.";
        }

        public TypeBase GetLambdaType()
        {
            if (GetRightTerm() == null || !TermManager.IsOfType(GetRightTerm(), TermTypes.Abstraction))
            {
                return new TypeBase();
            }
            else
            {
                return GetRightTerm().GetComponent<AbstractionBehavior>().GetLambdaTypeBody();
            }
        }
    }
}
