﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.TypeSystem;
using UnityEngine;

namespace Assets.Scripts
{
    interface ICalculatingAbstraction
    {
        GameObject CalculateOutput(GameObject input);

        TypeBase GetPredefinedTypeBody();
    }
}
