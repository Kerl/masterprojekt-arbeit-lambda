﻿using Assets.Scripts.TypeSystem;
using UnityEngine;

namespace Assets.Scripts.Terms
{
    public class EqualsBehavior : MonoBehaviour, ICalculatingAbstraction
    {
        public GameObject PrafabBoolean;

        // Use this for initialization
        void Start () {
	
        }
	
        // Update is called once per frame
        void Update () {
	
        }

        public GameObject CalculateOutput(GameObject input)
        {
            var left = transform.Find("Application").Find("Visuals").Find("LeftSpawn").GetComponent<SpawnBehavior>().CurrentGameObject;
            var right = transform.Find("Application").Find("Visuals").Find("RightSpawn").GetComponent<SpawnBehavior>().CurrentGameObject;

            var leftValue = left.GetComponent<NumberBehavior>().CurrentNumber;
            var rightValue = right.GetComponent<NumberBehavior>().CurrentNumber;

            var ret = Instantiate(PrafabBoolean);
            ret.GetComponent<BooleanBehavior>().SetCurrentValue(leftValue == rightValue);

            return ret;
        }

        public TypeBase GetPredefinedTypeBody()
        {
            return new TypeBoolean();
        }
    }
}
