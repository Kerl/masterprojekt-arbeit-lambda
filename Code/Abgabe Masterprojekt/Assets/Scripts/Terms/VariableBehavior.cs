﻿using Assets.Scripts.TypeSystem;
using Assets.Scripts.Utility;
using UnityEngine;

namespace Assets.Scripts
{
    public class VariableBehavior : MonoBehaviour, ITypedTerm
    {
        public int Id;
        public AbstractionBehavior BindingAbstraction;

        public TypeBase GetLambdaType()
        {
            if (BindingAbstraction != null)
            {
                return BindingAbstraction.GetLambdaTypeHead();
            }

            if (GetComponent<BooleanBehavior>() != null)
            {
                return new TypeBoolean();
            }

            if (GetComponent<NumberBehavior>() != null)
            {
                return new TypeNumber();
            }

            return null;
        }
    }
}
