﻿using Assets.Scripts.Terms;
using Assets.Scripts.TestCases;
using Assets.Scripts.TypeSystem;
using UnityEngine;

namespace Assets.Scripts
{
    public class BooleanBehavior : TypedTermMonoBehavior, IComparableTerm, ITypedTerm
    {
        public Sprite SpriteTrue;
        public Sprite SpriteFalse;
        public bool CurrentValue;

        // Use this for initialization
        void Start () {
	
        }
	
        // Update is called once per frame
        void Update () {
	
        }

        public void SetCurrentValue(bool newValue)
        {
            if (newValue)
            {
                transform.Find("Visuals").Find("Background").GetComponent<SpriteRenderer>().sprite = SpriteTrue;
            }
            else
            {
                transform.Find("Visuals").Find("Background").GetComponent<SpriteRenderer>().sprite = SpriteFalse;
            }

            CurrentValue = newValue;
        }

        public bool Compare(GameObject go)
        {
            var b = go.GetComponent<BooleanBehavior>();

            if (b != null && b.CurrentValue == CurrentValue)
            {
                return true;
            }

            return false;
        }

        public TypeBase GetLambdaType()
        {
            return new TypeBoolean();
        }
    }
}
