﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Terms
{
    interface IFillabeTerm
    {
        bool CheckForFilledSlots();

        void SpawnPlaceholders();
    }
}
