﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Terms
{
    public class TypedTermMonoBehavior : MonoBehaviour
    {
        public string PredefinedReturnType;

        public string GetReturnType()
        {
            if (PredefinedReturnType != "")
            {
                return PredefinedReturnType;
            }

            return GetReturnTypeInternal();
        }

        protected virtual string GetReturnTypeInternal()
        {
            throw new NotImplementedException(gameObject.name);
        }
    }
}
