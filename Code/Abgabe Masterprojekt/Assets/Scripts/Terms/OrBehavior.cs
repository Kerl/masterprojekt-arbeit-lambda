﻿using Assets.Scripts.TypeSystem;
using UnityEngine;

namespace Assets.Scripts
{
    public class OrBehavior : MonoBehaviour, ICalculatingAbstraction {

        // Use this for initialization
        void Start () {
	
        }
	
        // Update is called once per frame
        void Update () {
	
        }

        public GameObject CalculateOutput(GameObject input)
        {
            var left = transform.Find("Application").Find("Visuals").Find("LeftSpawn").GetComponent<SpawnBehavior>().CurrentGameObject;
            var right = transform.Find("Application").Find("Visuals").Find("RightSpawn").GetComponent<SpawnBehavior>().CurrentGameObject;

            var leftValue = left.GetComponent<BooleanBehavior>().CurrentValue;
            var rightValue = right.GetComponent<BooleanBehavior>().CurrentValue;

            left.GetComponent<BooleanBehavior>().SetCurrentValue(leftValue || rightValue);

            return left;
        }

        public TypeBase GetPredefinedTypeBody()
        {
            return new TypeBoolean();
        }
    }
}
