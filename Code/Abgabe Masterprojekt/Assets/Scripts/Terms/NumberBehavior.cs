﻿using Assets.Scripts.Terms;
using Assets.Scripts.TypeSystem;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class NumberBehavior : TypedTermMonoBehavior, ITypedTerm
    {
        public int CurrentNumber;

        public void SetNumber(int newNumber)
        {
            if (newNumber < 1)
            {
                CurrentNumber = 1;
            }
            else
            {
                CurrentNumber = newNumber;
            }

            transform.Find("UI").Find("CanvasNumber").Find("TextNumber").GetComponent<Text>().text = CurrentNumber.ToString();
        }

        public void Increase()
        {
            SetNumber(CurrentNumber + 1);
        }

        public void Decrease()
        {
            SetNumber(CurrentNumber - 1);
        }

        public TypeBase GetLambdaType()
        {
            return new TypeNumber();
        }
    }
}
