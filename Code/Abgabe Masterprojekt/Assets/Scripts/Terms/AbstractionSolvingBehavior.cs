﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Helper;
using Assets.Scripts.Utility;
using UnityEngine;

namespace Assets.Scripts
{
    public class AbstractionSolvingBehavior : MonoBehaviour
    {
        // Use this for initialization
        void Start () {
	
        }
	
        // Update is called once per frame
        void Update () {
	
        }

        public void OnTriggerEnter2D(Collider2D col)
        {
            StartAnimation();
            StartCoroutine(Wait(this, col));

            //PerformInsert(col);
        }

        public void PerformInsert(Collider2D col)
        {
            var solvingShouldContinue = true;

            GetComponent<BoxCollider2D>().isTrigger = false;
            LayerManager.ApplyLayerOffset(col.gameObject.transform, -2);
            LayerManager.Flatten(col.gameObject);

            var boundVariables = gameObject.GetComponent<AbstractionBehavior>().BoundVariables;
            var tempList = new List<GameObject>(boundVariables);

            foreach (var boundVariable in tempList)
            {
                boundVariable.GetComponent<TermBehavior>().CurrentSpawn.GetNewOb(col.gameObject, 4, 0.314f);
            }

            if (gameObject.GetComponent<ICalculatingAbstraction>() == null)
            {
                var spawn = transform.Find("Visuals").Find("LeftSpawn").GetComponent<SpawnBehavior>();
                var currentGameObject = spawn.CurrentGameObject;
                currentGameObject.transform.position = transform.parent.position;

                if (transform.parent.parent != null)
                {
                    currentGameObject.transform.SetParent(transform.parent.parent);
                }

                currentGameObject.transform.localScale = transform.parent.localScale;

                var p = transform.parent.parent;

                if (p != null && p.name != "Level")
                {
                    var targetSpawn = transform.parent.GetComponent<TermBehavior>().CurrentSpawn;
                    targetSpawn.CurrentGameObject = currentGameObject;
                    targetSpawn.CurrentColorIndex =
                        transform.parent.Find("Visuals").Find("RightSpawn").GetComponent<SpawnBehavior>().CurrentColorIndex;
                }
            }
            else
            {
                var spawn = transform.Find("Visuals").Find("LeftSpawn").GetComponent<SpawnBehavior>();
                var currentGameObject = gameObject.GetComponent<ICalculatingAbstraction>().CalculateOutput(col.gameObject);

                if (currentGameObject != null)
                {
                    currentGameObject.transform.position = transform.parent.position;

                    if (transform.parent.parent != null)
                    {
                        currentGameObject.transform.SetParent(transform.parent.parent);
                    }

                    currentGameObject.transform.localScale = transform.parent.localScale;

                    var p = transform.parent.parent;

                    if (p != null && p.name != "Level")
                    {
                        var targetSpawn = transform.parent.GetComponent<TermBehavior>().CurrentSpawn;
                        targetSpawn.CurrentGameObject = currentGameObject;
                        targetSpawn.CurrentColorIndex =
                            transform.parent.Find("Visuals").Find("RightSpawn").GetComponent<SpawnBehavior>().CurrentColorIndex;
                    }
                }
                else
                {
                    Destroy(col.gameObject);
                    TermManager.FindRootTerm().GetComponent<Animator>().SetTrigger("shake");

                    solvingShouldContinue = false;
                }
            }

            if (solvingShouldContinue)
            {
                Destroy(transform.parent.gameObject);
                col.gameObject.transform.parent = null;
                Destroy(col.gameObject);

                TermManager.UpdateTerm();
                TermManager.PerformCleanup();
                LayerManager.PerformLayerCleanup();

                GameObject.Find("Utility").GetComponent<TermSolver>().TrySolveNextStep();
            }
        }

        private void StartAnimation()
        {
            gameObject.GetComponent<AbstractionBehavior>().StartHeadAnimation();
        }

        IEnumerator Wait(AbstractionSolvingBehavior solv, Collider2D col)
        {
            yield return new WaitForSeconds(2.2f);

            solv.PerformInsert(col);
        }
    }
}
