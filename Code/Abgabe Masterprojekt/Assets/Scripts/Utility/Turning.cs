﻿using UnityEngine;

namespace Assets.Scripts.Utility
{
    public class Turning : MonoBehaviour
    {
        public float TurningSpeed = 100;

        void Start ()
        {
	
        }
	
        void Update ()
        {
            transform.eulerAngles += new Vector3(0,0,TurningSpeed * Time.deltaTime);

        }
    }
}
