﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Helper;
using Assets.Scripts.TestCases;
using UnityEngine;

namespace Assets.Scripts.Utility
{
    public class TermSolver : MonoBehaviour
    {
        public GameObject ApplicationToEval;
        public List<GameObject> _history;
        public int _currentHistoryIndex;
        public bool StaticTyping;
        private bool _isPaused;

        // Use this for initialization
        void Start()
        {
            _history = new List<GameObject>();
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.H))
            {
                if (ApplicationToEval != null)
                {
                    ApplicationToEval.GetComponent<ApplicationBehavior>().SetPulsing(false);
                }

                ApplicationToEval = TermManager.FindNextEval(GameObject.Find("Level").transform.GetChild(0).gameObject);

                if (ApplicationToEval != null)
                {
                    ApplicationToEval.GetComponent<ApplicationBehavior>().SetPulsing(true);
                }
            }

            if (Input.GetKeyDown(KeyCode.P))
            {
                PauseSolving();
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                LoadPreviousHistoryEntry();
            }

            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                LoadNextHistoryEntry();
            }
        }

        private void TrimHistory()
        {
            for (int i = _currentHistoryIndex; i < _history.Count; i++)
            {
                Destroy(_history[i]);
            }

            _history.RemoveRange(_currentHistoryIndex, _history.Count - _currentHistoryIndex);
        }

        public void StartSolving()
        {
            Debug.Log("Solving started.");

            if (!StaticTyping || (TermManager.CheckTreeForErrors() && TermManager.CheckTreeForTypes()))
            {
                if (GameObject.Find("Utility").transform.Find("Testing").Find("TestSubject").childCount == 0)
                {
                    var subject = Instantiate(TermManager.FindRootTerm());
                    subject.transform.SetParent(GameObject.Find("Utility").transform.Find("Testing").Find("TestSubject"));
                    subject.SetActive(false);
                }

                _isPaused = false;

                TrimHistory();
                TrySolveNextStep();
            }
        }

        public void PauseSolving()
        {
            Debug.Log("Solving paused.");
            _isPaused = true;
        }

        public void TrySolveNextStep()
        {
            if (!_isPaused)
            {
                StartCoroutine(WaitOneFrameBeforeNextStep(this));
            }
        }

        public void SolveNextStep()
        {
            TermManager.UpdateTerm();

            if (ApplicationToEval != null)
            {
                if (!StaticTyping)
                {
                    if (!TermManager.CheckSingleTermForErrors(ApplicationToEval) 
                        || !TermManager.CheckSingleTermForErrors(ApplicationToEval.GetComponent<ApplicationBehavior>().GetRightTerm()) 
                        || !TermManager.CheckSingleTermForErrors(ApplicationToEval.GetComponent<ApplicationBehavior>().GetLeftTerm()))
                    {
                        return;
                    }
                }

                var historyItem = Instantiate(TermManager.FindRootTerm());
                historyItem.transform.SetParent(transform);
                historyItem.SetActive(false);
                _history.Add(historyItem);
                _currentHistoryIndex++;

                ApplicationToEval.GetComponent<ApplicationBehavior>().StartSolving();
            }
            else
            {
                Debug.Log("Solving Finished!");

                var testResult = GameObject.Find("Utility").GetComponent<BackgroundSolver>().RunTestCases();
                var additionalTestResult = GameObject.Find("Utility").GetComponent<IAdditionalTestCaseCheck>().Check();

                if (testResult && additionalTestResult)
                {
                    GameObject.Find("Canvas").transform.Find("NextLevel").gameObject.SetActive(true);
                }
            }
        }

        public void ResetHistory()
        {
            foreach (var go in _history)
            {
                Destroy(go);
            }

            _history.Clear();
            _currentHistoryIndex = 0;
        }

        public void LoadPreviousHistoryEntry()
        {
            if (_currentHistoryIndex > 0)
            {
                _currentHistoryIndex--;
                GameObject.Find("Utility").GetComponent<Util>().LoadFromDuplicate(_history[_currentHistoryIndex]);
            }
        }

        public void LoadNextHistoryEntry()
        {
            if (_currentHistoryIndex < _history.Count - 1)
            {
                _currentHistoryIndex++;
                GameObject.Find("Utility").GetComponent<Util>().LoadFromDuplicate(_history[_currentHistoryIndex]);
            }
        }

        public void ToogleTyping()
        {
            StaticTyping = !StaticTyping;
        }

        IEnumerator WaitOneFrameBeforeNextStep(TermSolver solver)
        {
            yield return null;
            solver.SolveNextStep();
        }
    }
}
