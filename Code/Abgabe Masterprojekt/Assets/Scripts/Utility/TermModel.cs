﻿using Assets.Scripts.Helper;
using UnityEngine;

namespace Assets.Scripts.Utility
{
    public class TermModel
    {
        public int Id;
        public Sprite Texture;
        public GameObject Variable;
        public TermTypes Type;
    }
}
