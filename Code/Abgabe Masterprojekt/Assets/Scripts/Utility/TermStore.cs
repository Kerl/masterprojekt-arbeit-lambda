﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Helper;
using Assets.Scripts.UI;
using UnityEngine;

namespace Assets.Scripts.Utility
{
    public class TermStore : MonoBehaviour
    {
        public GameObject PrefabAbstraction;
        public GameObject PrefabApplication;
        public GameObject PrefabBoolean;
        public GameObject PrefabNumber;
        public GameObject PrefabNegation;
        public GameObject PrefabAnd;
        public GameObject PrefabOr;
        public GameObject PrefabPlus;
        public GameObject PrefabMinus;
        public GameObject PrefabMultiply;
        public GameObject PrefabEquals;
        public GameObject PrefabSmallerThan;
        public GameObject PrefabBiggerThan;
        public GameObject PrefabIfThenElse;
        public GameObject PrefabTypeBoolean;
        public GameObject PrefabTypeNumber;
        public GameObject PrefabTypeFunction;
        public List<TermModel> CurrentVariables;
        public List<TermModel> CurrentCopies;
        private int _lastId;
        private GameObject _termToAdd;
        private TermTypes _termType;
        private TermModel _termModel;
        private int _texSize = 250;
        private bool _isInitialized;
        private TermSidebarBehavior _sidebar;

        // Use this for initialization
        void Start()
        {
            _sidebar = GameObject.Find("Canvas").transform.Find("VariableSidebar").GetComponent<TermSidebarBehavior>();
            _lastId = 2;

            CurrentVariables = new List<TermModel>();
            CurrentCopies = new List<TermModel>();

            StartCoroutine(Initialize(this));
        }

        // Update is called once per frame
        void Update()
        {

        }

        public TermModel AddTerm(GameObject term, TermTypes type)
        {
            _termToAdd = term;
            _termType = type;
            _termModel = new TermModel();

            var g = GameObject.Instantiate(term);
            g.transform.position = new Vector3(500, 500, 0);
            g.transform.localScale = new Vector3(0.32f, 0.32f);
            g.SetActive(true);

            var oldCamPos = Camera.main.transform.position;
            var oldZoom = Camera.main.orthographicSize;
            Camera.main.GetComponent<CameraOnPostRender>().ShouldUpdateVariableStore = true;
            Camera.main.transform.position = new Vector3(500, 500, -10);
            Camera.main.orthographicSize = 5f;

            Camera.main.Render();

            LayerManager.Flatten(term);

            Camera.main.transform.position = oldCamPos;
            Camera.main.orthographicSize = oldZoom;

            GameObject.Destroy(g);

            return _termModel;
        }

        public void DeleteTerm(TermModel model)
        {
            CurrentVariables.Remove(model);
            CurrentCopies.Remove(model);
        }

        public void UpdateStore()
        {
            if (_termToAdd != null)
            {

                if (_termType == TermTypes.Variable)
                {
                    _termModel.Id = _lastId++;
                }
                else
                {
                    _termModel.Id = 0;
                }

                _termModel.Variable = _termToAdd;
                _termModel.Type = _termType;

                var tex = new Texture2D(_texSize, _texSize);
                tex.ReadPixels(new Rect(Screen.width / 2f - _texSize / 2f, Screen.height / 2f - _texSize / 2f, _texSize, _texSize), 0, 0);
                tex.Apply();

                var fillColorArray = tex.GetPixels();
                var backColor = fillColorArray[0];
                for (var i = 0; i < fillColorArray.Length; ++i)
                {
                    if (fillColorArray[i] == backColor)
                    {
                        fillColorArray[i] = Color.gray;
                    }

                }
                tex.SetPixels(fillColorArray);
                tex.Apply();

                var sprite = Sprite.Create(tex, new Rect(0f, 0f, tex.height, tex.width), new Vector2(0.5f, 0.5f), 100.0f);
                _termModel.Texture = sprite;


                if (_termModel.Type == TermTypes.Clipboard)
                {
                    CurrentCopies.Add(_termModel);
                }
                else
                {
                    CurrentVariables.Add(_termModel);
                }

                _termToAdd = null;
            }
        }

        IEnumerator Initialize(TermStore store)
        {
            var ob = Instantiate(store.PrefabApplication);
            store.AddTerm(ob, TermTypes.Application);
            ob.transform.SetParent(store.gameObject.transform);
            ob.SetActive(false);
            yield return null;

            ob = Instantiate(store.PrefabAbstraction);
            store.AddTerm(ob, TermTypes.Abstraction);
            ob.transform.SetParent(store.gameObject.transform);
            ob.SetActive(false);
            yield return null;

            ob = Instantiate(store.PrefabBoolean);
            store.AddTerm(ob, TermTypes.Abstraction);
            ob.transform.SetParent(store.gameObject.transform);
            ob.SetActive(false);
            yield return null;

            ob = Instantiate(store.PrefabNumber);
            store.AddTerm(ob, TermTypes.Abstraction);
            ob.transform.SetParent(store.gameObject.transform);
            ob.SetActive(false);
            yield return null;

            ob = Instantiate(store.PrefabNegation);
            store.AddTerm(ob, TermTypes.Abstraction);
            ob.transform.SetParent(store.gameObject.transform);
            ob.SetActive(false);
            yield return null;

            ob = Instantiate(store.PrefabAnd);
            store.AddTerm(ob, TermTypes.Abstraction);
            ob.transform.SetParent(store.gameObject.transform);
            ob.SetActive(false);
            yield return null;

            ob = Instantiate(store.PrefabOr);
            store.AddTerm(ob, TermTypes.Abstraction);
            ob.transform.SetParent(store.gameObject.transform);
            ob.SetActive(false);
            yield return null;

            ob = Instantiate(store.PrefabPlus);
            store.AddTerm(ob, TermTypes.Abstraction);
            ob.transform.SetParent(store.gameObject.transform);
            ob.SetActive(false);
            yield return null;

            ob = Instantiate(store.PrefabMinus);
            store.AddTerm(ob, TermTypes.Abstraction);
            ob.transform.SetParent(store.gameObject.transform);
            ob.SetActive(false);
            yield return null;

            ob = Instantiate(store.PrefabMultiply);
            store.AddTerm(ob, TermTypes.Abstraction);
            ob.transform.SetParent(store.gameObject.transform);
            ob.SetActive(false);
            yield return null;

            ob = Instantiate(store.PrefabEquals);
            store.AddTerm(ob, TermTypes.Abstraction);
            ob.transform.SetParent(store.gameObject.transform);
            ob.SetActive(false);
            yield return null;

            ob = Instantiate(store.PrefabSmallerThan);
            store.AddTerm(ob, TermTypes.Abstraction);
            ob.transform.SetParent(store.gameObject.transform);
            ob.SetActive(false);
            yield return null;

            ob = Instantiate(store.PrefabBiggerThan);
            store.AddTerm(ob, TermTypes.Abstraction);
            ob.transform.SetParent(store.gameObject.transform);
            ob.SetActive(false);
            yield return null;

            ob = Instantiate(store.PrefabIfThenElse);
            store.AddTerm(ob, TermTypes.Abstraction);
            ob.transform.SetParent(store.gameObject.transform);
            ob.SetActive(false);
            yield return null;

            ob = Instantiate(store.PrefabTypeBoolean);
            store.AddTerm(ob, TermTypes.TypeInformation);
            ob.transform.SetParent(store.gameObject.transform);
            ob.SetActive(false);
            yield return null;

            ob = Instantiate(store.PrefabTypeNumber);
            store.AddTerm(ob, TermTypes.TypeInformation);
            ob.transform.SetParent(store.gameObject.transform);
            ob.SetActive(false);
            yield return null;

            ob = Instantiate(store.PrefabTypeFunction);
            store.AddTerm(ob, TermTypes.TypeInformation);
            ob.transform.SetParent(store.gameObject.transform);
            ob.SetActive(false);
            yield return null;
        }
    }
}
