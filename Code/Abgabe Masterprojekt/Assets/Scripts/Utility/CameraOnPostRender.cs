﻿using UnityEngine;

namespace Assets.Scripts.Utility
{
    public class CameraOnPostRender : MonoBehaviour
    {
        public bool ShouldUpdateVariableStore;
        private TermStore _termStore;

        public void Start()
        {
            _termStore = GameObject.Find("Utility").GetComponent<TermStore>();
        }

        public void OnPostRender()
        {
            if (ShouldUpdateVariableStore)
            {
                _termStore.UpdateStore();
                ShouldUpdateVariableStore = false;
            }
        }
    }
}
