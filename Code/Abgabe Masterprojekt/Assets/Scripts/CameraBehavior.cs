﻿using UnityEngine;

namespace Assets.Scripts
{
    public class CameraBehavior : MonoBehaviour
    {
        public bool DragEnabled;
        public bool ZoomEnabled;
        private const float ZoomSpeed = 0.5f;
        private const float MaxZoom = 0.1f;
        private const float DragSpeed = 0.02f;
        private bool _isDragging;
        private Vector3 _lastDragPosition;

        // Use this for initialization
        private void Start()
        {
            DragEnabled = true;
            ZoomEnabled = true;
        }

        // Update is called once per frame
        private void Update()
        {
            PerformDrag();
            PerformZoom();
        }

        private void PerformZoom()
        {
            if (ZoomEnabled)
            {
#if UNITY_ANDROID
                var camera = Camera.main;

                if (Input.touchCount == 2)
                {
                    Touch touchZero = Input.GetTouch(0);
                    Touch touchOne = Input.GetTouch(1);

                    Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                    Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                    float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                    float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

                    float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

                    camera.orthographicSize += deltaMagnitudeDiff*ZoomSpeed*0.1f;

                    camera.orthographicSize = Mathf.Max(camera.orthographicSize, MaxZoom);
                }
#endif

//#if UNITY_EDITOR
                Camera.main.orthographicSize -= Input.mouseScrollDelta.y*ZoomSpeed;

                if (Camera.main.orthographicSize < MaxZoom)
                {
                    Camera.main.orthographicSize = MaxZoom;
                }
//#endif
            }
        }

        private void PerformDrag()
        {
            if (DragEnabled)
            {
#if UNITY_ANDROID
                if (Input.touchCount > 1)
                {
                    _isDragging = false;
                    return;
                }
#endif

                if (Input.GetMouseButtonDown(0))
                {
                    _isDragging = true;
                    _lastDragPosition = Input.mousePosition;
                }


                if (Input.GetMouseButtonUp(0))
                {
                    _isDragging = false;
                }

                if (_isDragging)
                {
                    var dif = _lastDragPosition - Input.mousePosition;
                    Camera.main.transform.position += dif*Camera.main.orthographicSize/250;

                    _lastDragPosition = Input.mousePosition;
                }
            }
        }
    }
}