﻿using System.Collections.Generic;
using Assets.Scripts.Helper;
using UnityEngine;

namespace Assets.Scripts
{
    public class TypeInformationRoot : MonoBehaviour
    {
        public GameObject PrefabIndicator;
        public Vector3 RootPos;
        public List<GameObject> TypeInformation;
        private bool _isEditable;
        private GameObject _activeTypeInfo;

        // Use this for initialization
        void Start ()
        {
            RootPos = transform.position;
        }
	
        // Update is called once per frame
        void Update () {
	
        }
        
        public bool IsEditable()
        {
            return _isEditable;
        }

        //public void EnableEditMode()
        //{
        //    _isEditable = true;

        //    if (TypeInformation.Count != 0)
        //    {
        //        _activeTypeInfo = TypeInformation[TypeInformation.Count-1];
        //    }

        //    TypeInformation.Add(PrefabIndicator);
        //    PrefabIndicator.SetActive(true);
        //    TypeInformationManager.IndentTypeInfo(this);
        //}

        //public void DisableEditMode()
        //{
        //    _isEditable = false;
        //    TypeInformation.Remove(PrefabIndicator);
        //    PrefabIndicator.SetActive(false);
        //    TypeInformationManager.IndentTypeInfo(this);
        //}

        //public void InsertTypeInfo(GameObject typeInfo)
        //{
        //    if (TypeInformation.FindIndex(t => t == PrefabIndicator) != -1)
        //    {
        //        TypeInformation.Insert(TypeInformation.FindIndex(t => t == PrefabIndicator), typeInfo);
        //    }
        //    else
        //    {
        //        TypeInformation.Add(typeInfo);
        //    }

        //    TypeInformationManager.IndentTypeInfo(this);
        //    CheckTypeStructure();
        //}

        //public void RemoveTypeInfo(GameObject element)
        //{
        //    if (TypeInformation.FindIndex(t => t == element) > 0)
        //    {
        //        var arrow = TypeInformation[TypeInformation.FindIndex(t => t == element) - 1];
        //        TypeInformation.Remove(arrow);
        //        Destroy(arrow);
        //    }

        //    TypeInformation.Remove(element);  
        //    TypeInformationManager.IndentTypeInfo(this);
        //    CheckTypeStructure();
        //}

        //public void SetActiveTypeInfo(GameObject typeInfo)
        //{
        //    _activeTypeInfo = typeInfo;

        //    TypeInformation.Remove(PrefabIndicator);
        //    TypeInformation.Insert(TypeInformation.FindIndex(t => t == typeInfo) + 1, PrefabIndicator);
        //    PrefabIndicator.SetActive(true);
        //    TypeInformationManager.IndentTypeInfo(this);
        //}

        //public void IncreaseRankOfActiveInfo()
        //{
        //    if (_activeTypeInfo != null)
        //    {
        //        _activeTypeInfo.GetComponent<TypeInformationBehavior>().IncreaseRank();

        //        IncreaseRankOfRightArrowIfNecessary();
        //    }
        //}

        //private void IncreaseRankOfRightArrowIfNecessary()
        //{
        //    var arrow = FindNextTypeInfoRight(TypeInformation.FindIndex(t => t == _activeTypeInfo));

        //    if (arrow != null)
        //    {
        //        var info = FindNextTypeInfoRight(TypeInformation.FindIndex(t => t == arrow));

        //        if (info != null)
        //        {
        //            if (_activeTypeInfo.GetComponent<TypeInformationBehavior>().Rank == info.GetComponent<TypeInformationBehavior>().Rank &&
        //                _activeTypeInfo.GetComponent<TypeInformationBehavior>().Rank > arrow.GetComponent<TypeInformationBehavior>().Rank)
        //            {
        //                arrow.GetComponent<TypeInformationBehavior>().IncreaseRankInternal(false);
        //            }
        //        }
        //    }
        //}

        //public void DecreaseRankOfActiveInfo()
        //{
        //    if (_activeTypeInfo != null)
        //    {
        //        _activeTypeInfo.GetComponent<TypeInformationBehavior>().DecreaseRank();

        //        DecreaseRankOfRightArrowIfNecessary();
        //    }
        //}

        //private void DecreaseRankOfRightArrowIfNecessary()
        //{
        //    var arrow = FindNextTypeInfoRight(TypeInformation.FindIndex(t => t == _activeTypeInfo));

        //    if (arrow != null &&
        //        _activeTypeInfo.GetComponent<TypeInformationBehavior>().Rank < arrow.GetComponent<TypeInformationBehavior>().Rank)
        //    {
        //        arrow.GetComponent<TypeInformationBehavior>().DecreaseRankInternal(false);
        //    }
        //}

        //public GameObject FindNextTypeInfoLeft(int index)
        //{
        //    if (index <= 0)
        //    {
        //        return null;
        //    }

        //    if (TypeInformation[index - 1].GetComponent<TypeInformationBehavior>() != null)
        //    {
        //        return TypeInformation[index - 1];
        //    }
        //    else if (TypeInformation[index - 2].GetComponent<TypeInformationBehavior>() != null)
        //    {
        //        return TypeInformation[index - 2];
        //    }

        //    return null;
        //}

        //public GameObject FindNextTypeInfoRight(int index)
        //{
        //    if (index >= TypeInformation.Count - 1)
        //    {
        //        return null;
        //    }

        //    if (TypeInformation[index + 1].GetComponent<TypeInformationBehavior>() != null)
        //    {
        //        return TypeInformation[index + 1];
        //    }
        //    else if ((index + 2) < (TypeInformation.Count - 1) && TypeInformation[index + 2].GetComponent<TypeInformationBehavior>() != null)
        //    {
        //        return TypeInformation[index + 2];
        //    }

        //    return null;
        //}

        //public void CheckTypeStructure()
        //{
        //    if (TypeInformation.Count > 1)
        //    {
        //        var i = 1;
        //        var numberOfSameRanks = 1;
        //        var type = TypeInformation[0];
        //        var rank = type.GetComponent<TypeInformationBehavior>().Rank;

        //        ResetWarnings();

        //        while (FindNextTypeInfoRight(i) != null)
        //        {
        //            type = FindNextTypeInfoRight(i);

        //            if (type.name.Contains("arrow"))
        //            {
        //                i++;
        //                continue;
        //            }

        //            if (rank == type.GetComponent<TypeInformationBehavior>().Rank)
        //            {
        //                numberOfSameRanks++;

        //                if (numberOfSameRanks >= 3)
        //                {
        //                    ShowWarningForRank(rank);
        //                }
        //            }
        //            else
        //            {
        //                rank = type.GetComponent<TypeInformationBehavior>().Rank;
        //                numberOfSameRanks = 1;
        //            }

        //            i++;
        //        }
        //    }
        //}

        //private void ShowWarningForRank(int rank)
        //{
        //    var i = 0;

        //    foreach (var typeInfo in TypeInformation)
        //    {
        //        if (typeInfo.name != "PositionIndicator" && 
        //            typeInfo.GetComponent<TypeInformationBehavior>().Rank == rank)
        //        {
        //            typeInfo.GetComponent<TypeInformationBehavior>().ShowWarningBorder();
        //        }
        //    }
        //}

        //private void ResetWarnings()
        //{
        //    foreach (var typeInfo in TypeInformation)
        //    {
        //        if (typeInfo.name != "PositionIndicator")
        //        {
        //            typeInfo.GetComponent<TypeInformationBehavior>().HideWarningBorder();
        //        }
        //    }
        //}
    }
}
