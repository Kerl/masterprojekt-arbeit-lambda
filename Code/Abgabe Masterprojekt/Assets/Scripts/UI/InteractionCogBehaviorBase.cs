﻿using System;
using System.Collections;
using Assets.Scripts.Helper;
using Assets.Scripts.Utility;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public class InteractionCogBehaviorBase : MonoBehaviour
    {
        public GameObject UiCanvas;
        public bool IsMinimized;
        protected Coroutine _currentCoroutine;
        protected Turning _turning;

        // Use this for initialization
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
        }

        private void OnMouseEnter()
        {
            //Debug.Log("Enter!");

            //if (!IsMinimized)
            //{
            //    _turning.TurningSpeed = -50;

            //    if (_currentCoroutine != null)
            //    {
            //        StopCoroutine(_currentCoroutine);
            //    }
            //    //StartCoroutine(Coroutines.CR_DoScale(new Vector3(0.25f, 0.25f), new Vector3(0.35f, 0.35f), 0.1f, gameObject, null));
            //}
        }

        private void OnMouseExit()
        {
            //Debug.Log("Exit!");

            //if (!IsMinimized)
            //{
            //    _turning.TurningSpeed = -25;

            //    if (_currentCoroutine != null)
            //    {
            //        StopCoroutine(_currentCoroutine);
            //    }
            //    //StartCoroutine(Coroutines.CR_DoScale(new Vector3(0.35f, 0.35f), new Vector3(0.25f, 0.25f), 0.1f, gameObject, null));
            //}
        }
    }
}