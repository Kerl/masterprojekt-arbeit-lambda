﻿using Assets.Scripts.Helper;
using Assets.Scripts.Utility;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class PaintableBehavior : MonoBehaviour
    {
        public Texture2D Brush;
        public Texture2D BlankSprite;
        public GameObject _prefabVariable;
        private bool _mousePressedLastFrame;
        private Vector2 _lastMousePosInPixels = new Vector2(-1, -1);
        private HeadBehavior _headBehavior;
        private TermStore _termStore;
        private bool _canBePainted;


        // Use this for initialization
        void Start()
        {
            _headBehavior = transform.parent.parent.Find("HeadSpawn").GetComponent<HeadBehavior>();
            _termStore = GameObject.Find("Utility").GetComponent<TermStore>();
            _canBePainted = true;
        }

        // Update is called once per frame
        void Update()
        {
        }

        public void OnMouseOver()
        {
            if (Input.GetMouseButton(0) && _canBePainted)
            {
                var renderer = GetComponent<SpriteRenderer>();
                var mousePosInWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
                var mousePosInPixels = renderer.sprite.pivot +
                                       new Vector2(mousePosInWorld.x, mousePosInWorld.y) * renderer.sprite.pixelsPerUnit / transform.lossyScale.x;

                if (!_mousePressedLastFrame)
                {
                    _lastMousePosInPixels = mousePosInPixels;
                }

                var diameter = Brush.height;
                var pixelArrayBrush = Brush.GetPixels();
                var pixelDelta = mousePosInPixels - _lastMousePosInPixels;
                var pixelDeltaNormalized = pixelDelta.normalized*3;
                var deltaLength = pixelDelta.magnitude;

                while (deltaLength > 0)
                {
                    PaintBrush(pixelArrayBrush, diameter, renderer, _lastMousePosInPixels + pixelDelta);
                    pixelDelta -= pixelDeltaNormalized;
                    deltaLength -= 3;
                }

                PaintBrush(pixelArrayBrush, diameter, renderer, mousePosInPixels);
                renderer.sprite.texture.Apply();

                _lastMousePosInPixels = mousePosInPixels;
                _mousePressedLastFrame = true;
            }
            else
            {
                _mousePressedLastFrame = false;
            }
        }

        private void PaintBrush(Color[] pixelArrayBrush, int diameter, SpriteRenderer renderer, Vector2 mousePosInPixels)
        {
            for (int i = 0; i < pixelArrayBrush.Length; i++)
            {
                if (pixelArrayBrush[i] != Color.black)
                {
                    var x = i%diameter;
                    var y = i/diameter;

                    pixelArrayBrush[i] = renderer.sprite.texture.GetPixel(
                        (int) mousePosInPixels.x + x - diameter/2, (int) mousePosInPixels.y + y - diameter/2);
                }
            }

            renderer.sprite.texture.SetPixels((int)mousePosInPixels.x - diameter / 2,
                (int)mousePosInPixels.y - diameter / 2, diameter, diameter, pixelArrayBrush);
        }

        public void OnMouseEnter()
        {
            Debug.Log("ASDFASDASDA");
            Camera.main.GetComponent<CameraBehavior>().DragEnabled = false;

            foreach (var boundVariable in transform.parent.parent.parent.GetComponent<AbstractionBehavior>().BoundVariables)
            {
                boundVariable.transform.Find("Visuals").Find("VariablePointer").gameObject.SetActive(true);
            }
        }

        public void OnMouseExit()
        {
            Camera.main.GetComponent<CameraBehavior>().DragEnabled = true;

            foreach (var boundVariable in transform.parent.parent.parent.GetComponent<AbstractionBehavior>().BoundVariables)
            {
                boundVariable.transform.Find("Visuals").Find("VariablePointer").gameObject.SetActive(false);
            }

            _mousePressedLastFrame = false;
        }

        public void MinimizePaintable()
        {
            StartCoroutine(Coroutines.CR_DoScale(new Vector3(0.32f, 0.32f), Vector3.zero, 0.1f, gameObject, new CompletionJobDisable()));
        }

        public void ResetPaintable()
        {
            var renderer = GetComponent<SpriteRenderer>(); ;
            var tex = (Texture2D)GameObject.Instantiate(BlankSprite);
            renderer.sprite = Sprite.Create(tex, renderer.sprite.rect, new Vector2(0.5f, 0.5f));

            _canBePainted = true;
        }

        public void SavePaintable()
        {
            var v = Instantiate(_prefabVariable);
            v.transform.Find("Visuals").Find("Background").GetComponent<SpriteRenderer>().sprite =
                GetComponent<SpriteRenderer>().sprite;
            v.transform.SetParent(_termStore.gameObject.transform);

            v.SetActive(false);

            var variableModel = _termStore.AddTerm(v, TermTypes.Variable);
            _headBehavior.CurrentVariableId = variableModel.Id;
            v.GetComponent<VariableBehavior>().Id = variableModel.Id;

            _canBePainted = false;
        }
    }
}