﻿using UnityEngine;

namespace Assets.Scripts.UI
{
    public class WorldCanvasBehaviorHead : WorldCanvasBehaviorBase {

        // Use this for initialization
        void Start () {
	
        }
	
        // Update is called once per frame
        void Update () {

            WasActive = gameObject.activeSelf;
        }

        public void MinimizeCanvas()
        {
            Minimize();
        }
    }
}
