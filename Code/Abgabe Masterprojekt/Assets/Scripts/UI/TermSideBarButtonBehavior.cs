﻿using System.Collections;
using Assets.Scripts.Helper;
using Assets.Scripts.Utility;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.UI
{
    public class TermSideBarButtonBehavior : MonoBehaviour, IPointerClickHandler
    {
        public TermModel Model;
        public GameObject PrefabArrow;
        private TermStore _termStore;

        // Use this for initialization
        void Start()
        {
            _termStore = GameObject.Find("Utility").GetComponent<TermStore>();
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Left)
            {
                float scale;
                if (Model.Type == TermTypes.TypeInformation)
                {
                    scale = 0.314f;
                }
                else
                {
                    scale = 0.314f;
                }

                var spawn = transform.parent.parent.GetComponent<TermSidebarBehavior>().CurrentActiveSpawn;
                spawn.GetNewOb(Model.Variable, 4, scale);
            }
            else if (eventData.button == PointerEventData.InputButton.Right)
            {
                if (Model.Type == TermTypes.Clipboard)
                {
                    _termStore.DeleteTerm(Model);
                }
            }
        }

        IEnumerator Spawn(TermSideBarButtonBehavior buttonBehavior, float scale)
        {
            var spawn = buttonBehavior.transform.parent.parent.GetComponent<TermSidebarBehavior>().CurrentActiveSpawn;

            if (Model.Type == TermTypes.TypeInformation &&
                spawn.GetComponent<TypeInformationRoot>().TypeInformation.Count > 1)
            {
                spawn.GetNewOb(PrefabArrow, 4, scale);
                yield return null;
            }

            spawn.GetNewOb(Model.Variable, 4, scale);
        }
    }
}
