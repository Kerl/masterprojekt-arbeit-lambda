﻿using UnityEngine;

namespace Assets.Scripts.UI
{
    public class ActivateCanvasOnDestroy : MonoBehaviour
    {
        public GameObject CanvasToActivate;

        private void OnDestroy()
        {
            CanvasToActivate.SetActive(true);
        }
    }
}
