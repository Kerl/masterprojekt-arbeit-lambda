﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Helper;
using Assets.Scripts.Utility;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class TermSidebarBehavior : MonoBehaviour
    {
        public GameObject PrefabSidebarButton;
        public bool IsMouseOver;
        public SpawnBehavior CurrentActiveSpawn;
        private TermStore _termStore;
        private CameraBehavior _cameraBehavior;

        // Use this for initialization
        void Start ()
        {
            _termStore = GameObject.Find("Utility").GetComponent<TermStore>();
            _cameraBehavior = Camera.main.GetComponent<CameraBehavior>();

            if (_termStore == null)
            {
                throw new MissingComponentException("TermStore");
            }
            if (_cameraBehavior == null)
            {
                throw new MissingComponentException("CameraBehavior");
            }
        }
	
        // Update is called once per frame
        void Update () {
            
            PerformScroll();
        }

        void LateUpdate()
        {
            CheckForMouseOver();
        }

        public void UpdateSidebar(List<int> variablesToShow, List<TermTypes> termTypesToShow)
        {
            if (transform.Find("Variables") != null)
            {
                Destroy(transform.Find("Variables").gameObject);
            }

            var variables = new GameObject();
            variables.name = "Variables";
            variables.transform.SetParent(transform);
            variables.transform.localPosition = Vector3.zero;

            var items = new List<TermModel>();

            foreach (var termModel in _termStore.CurrentVariables)
            {
                if (variablesToShow.Contains(termModel.Id) || termTypesToShow.Contains(termModel.Type)/*|| termModel.Type == TermTypes.Abstraction || termModel.Type == TermTypes.Application*/)
                {
                    items.Add(termModel);
                }
            }

            items.AddRange(_termStore.CurrentCopies);

            for (int i = 0; i < items.Count; i++)
            {
                var newButton = Instantiate(PrefabSidebarButton);
                newButton.transform.SetParent(variables.transform);
                newButton.GetComponent<Image>().sprite = items[i].Texture;

                newButton.GetComponent<RectTransform>().anchoredPosition = new Vector3(80, -80- 160*i);

                newButton.GetComponent<TermSideBarButtonBehavior>().Model = items[i];
            }
        }

        public void ShowTermSidebar(SpawnBehavior spawn)
        {
            CurrentActiveSpawn = spawn;

            var variableList = TermManager.FindParentAbstractions(spawn.gameObject);

            var typeList = new List<TermTypes>();
            typeList.Add(TermTypes.Abstraction);
            typeList.Add(TermTypes.Application);

            UpdateSidebar(variableList, typeList);
            gameObject.SetActive(true);
        }

        public void ShowTypeSidebar(SpawnBehavior spawn)
        {
            CurrentActiveSpawn = spawn;

            var variableList = new List<int>();

            var typeList = new List<TermTypes>();
            typeList.Add(TermTypes.TypeInformation);

            UpdateSidebar(variableList, typeList);
            gameObject.SetActive(true);
        }

        private void CheckForMouseOver()
        {
            if (Input.mousePosition.x <= 160)
            {
                _cameraBehavior.ZoomEnabled = false;
                IsMouseOver = true;
            }
            else
            {
                _cameraBehavior.ZoomEnabled = true;
                IsMouseOver = false;
            }
        }

        private void PerformScroll()
        {
            if (IsMouseOver)
            {
                var rectTransform = GetComponent<RectTransform>();

                rectTransform.anchoredPosition = new Vector2(0, rectTransform.anchoredPosition.y + Input.mouseScrollDelta.y * 50);

                if (rectTransform.anchoredPosition.y < 0)
                {
                    rectTransform.anchoredPosition = Vector2.zero;
                }
                else if (rectTransform.anchoredPosition.y > (_termStore.CurrentVariables.Count + _termStore.CurrentCopies.Count) * 160 - 320)
                {
                    rectTransform.anchoredPosition = new Vector2(0, (_termStore.CurrentVariables.Count + _termStore.CurrentCopies.Count) * 160 - 320);
                }
            }
        }
    }
}
