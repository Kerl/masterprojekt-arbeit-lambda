﻿using Assets.Scripts.Helper;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public class WorldCanvasBehavior : WorldCanvasBehaviorBase
    {
        // Use this for initialization
        void Start()
        {
            _termSideBar = GameObject.Find("Canvas").transform.Find("VariableSidebar").gameObject;
        }

        // Update is called once per frame
        void Update()
        {
            if (!_isMouseOver && !_termSideBar.GetComponent<TermSidebarBehavior>().IsMouseOver && WasActive && Input.GetMouseButtonDown(0))
            {
                Minimize();
            }

            WasActive = gameObject.activeSelf;
        }
    }
}