﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.UI
{
    public class Background : MonoBehaviour
    {

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnMouseDown()
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                var sidebar = GameObject.Find("Canvas").transform.Find("VariableSidebar");
                sidebar.gameObject.SetActive(false);
            }
        }
    }
}
