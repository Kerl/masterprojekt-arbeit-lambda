﻿using Assets.Scripts.Helper;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class SetTransparencyOnStart : MonoBehaviour {

        // Use this for initialization
        void Start ()
        {
            var image = GetComponent<Image>();

            if (image != null)
            {
                image.SetTransparency(0.5f);
            }
        }
    }
}
