﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class ButtonToggleDesription : MonoBehaviour
    {
        public string FirstDescription;
        public string SecondDescription;
        private Text _text;

        private void Start()
        {
            _text = transform.Find("Text").GetComponent<Text>();
            _text.text = FirstDescription;
        }

        public void ToggleDescription()
        {
            if (_text.text == FirstDescription)
            {
                _text.text = SecondDescription;
            }
            else
            {
                _text.text = FirstDescription;
            }
        }
    }
}
