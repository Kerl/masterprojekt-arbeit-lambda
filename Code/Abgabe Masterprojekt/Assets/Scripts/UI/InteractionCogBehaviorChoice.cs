﻿using Assets.Scripts.Utility;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public class InteractionCogBehaviorChoice: InteractionCogBehaviorBase
    {
        // Use this for initialization
        void Start()
        {
            _turning = GetComponent<Turning>();

            if (_turning == null)
            {
                throw new MissingComponentException("Turning.");
            }
        }

        // Update is called once per frame
        void Update()
        {
        }

        private void OnMouseDown()
        {
            IsMinimized = true;

            if (_currentCoroutine != null)
            {
                StopCoroutine(_currentCoroutine);
            }
            //StartCoroutine(Coroutines.CR_DoScale(new Vector3(0.35f, 0.35f), Vector3.zero, 0.1f, gameObject, new CompletionJobDisable()));
            //StartCoroutine(Coroutines.CR_DoScale(Vector3.zero, Vector3.one, 0.1f, UiCanvas, null));

            gameObject.SetActive(false);
            UiCanvas.SetActive(true);

            UiCanvas.GetComponent<WorldCanvasBehavior>().WasActive = false;
            UiCanvas.gameObject.SetActive(true);
        }
    }
}