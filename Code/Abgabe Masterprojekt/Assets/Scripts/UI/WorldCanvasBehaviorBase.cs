﻿using Assets.Scripts.Helper;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public class WorldCanvasBehaviorBase : MonoBehaviour
    {
        public bool WasActive;
        public GameObject InteractionCog;
        protected bool _isMouseOver;
        protected GameObject _termSideBar;

        // Use this for initialization
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
        }

        public void OnMouseEnter()
        {
            _isMouseOver = true;
        }

        public void OnMouseExit()
        {
            _isMouseOver = false;
        }

        protected void Minimize()
        {
            gameObject.SetActive(false);
            InteractionCog.GetComponent<InteractionCogBehaviorBase>().IsMinimized = false;
            InteractionCog.transform.localScale = new Vector3(0.25f, 0.25f);
            InteractionCog.SetActive(true);

            //if (_termSideBar != null)
            //{
            //    _termSideBar.SetActive(false);
            //}
        }
    }
}