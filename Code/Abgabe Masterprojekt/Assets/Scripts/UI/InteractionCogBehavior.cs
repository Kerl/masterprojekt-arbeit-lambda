﻿using Assets.Scripts.Utility;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public class InteractionCogBehavior : InteractionCogBehaviorBase
    {
        // Use this for initialization
        void Start()
        {
            _turning = GetComponent<Turning>();

            if (_turning == null)
            {
                throw new MissingComponentException("Turning.");
            }
        }

        // Update is called once per frame
        void Update()
        {
        }

        private void OnMouseDown()
        {
            IsMinimized = true;

            if (_currentCoroutine != null)
            {
                StopCoroutine(_currentCoroutine);
            }

            gameObject.SetActive(false);
            UiCanvas.SetActive(true);

            UiCanvas.GetComponent<WorldCanvasBehavior>().WasActive = false;
            UiCanvas.gameObject.SetActive(true);

            var sidebar = GameObject.Find("Canvas").transform.Find("VariableSidebar");
            sidebar.GetComponent<TermSidebarBehavior>().ShowTermSidebar(transform.parent.GetComponent<SpawnBehavior>());
        }
    }
}