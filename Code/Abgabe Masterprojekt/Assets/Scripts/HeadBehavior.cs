﻿using Assets.Scripts.Helper;
using Assets.Scripts.Utility;
using UnityEngine;

namespace Assets.Scripts
{
    public class HeadBehavior : MonoBehaviour
    {
        public GameObject Head;
        public int CurrentColorIndex = -1;
        public int CurrentVariableId;

        // Use this for initialization
        void Start () {
	
        }
	
        // Update is called once per frame
        void Update () {
	
        }

        public void ChangeHead(int colorIndex)
        {
            CurrentColorIndex = colorIndex;

            var newHead = (GameObject) Instantiate(Resources.Load("prefab_abstraction_head"), transform.position, Quaternion.identity);
            newHead.name = "Head";
            newHead.transform.SetParent(transform);
            newHead.transform.GetComponent<SpriteRenderer>().sortingOrder =
                transform.parent.Find("Background").GetComponent<SpriteRenderer>().sortingOrder + 4;
            newHead.transform.GetComponent<SpriteRenderer>().color = VariableColors.GetColor(colorIndex);

            StartCoroutine(Coroutines.CR_DoScale(Head.transform.localScale, Vector3.zero, 0.1f, Head,
                new CompletionJobDelete()));
            StartCoroutine(Coroutines.CR_DoScale(Vector3.zero, Head.transform.localScale, 0.1f, newHead, null));

            Head = newHead;
        }
    }
}
