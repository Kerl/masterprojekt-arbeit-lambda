﻿using System;
using System.Reflection;
using Assets.Scripts.Helper;
using Assets.Scripts.Utility;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts
{
    public class Util : MonoBehaviour
    {
        private GameObject _savedState;
        private TermSolver _solver;

        // Use this for initialization
        private void Start ()
        {
            _solver = gameObject.GetComponent<TermSolver>();
        }
	
        // Update is called once per frame
        private void Update ()
        {
            if (Input.GetKeyDown("x"))
            {
                ReloadLevel();
            }
        }

        public void ReloadLevel()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        public void LoadLevel(string level)
        {
            SceneManager.LoadScene(level);
        }

        public void SaveState()
        {
            if (_savedState != null)
            {
                Destroy(_savedState);
            }

            _savedState = GetDuplicateFromRoot();
            _savedState.transform.SetParent(gameObject.transform);
        }

        public void LoadState()
        {
            if (_savedState != null)
            {
                LoadFromDuplicate(_savedState);
                _solver.ResetHistory();
            }
        }

        public void LoadFromDuplicate(GameObject duplicate)
        {
            Destroy(GameObject.Find("Level").transform.GetChild(0).gameObject);

            GameObject d = Instantiate(duplicate);
            d.name = _savedState.name.Replace("(Clone)", "");
            d.transform.SetParent(GameObject.Find("Level").transform);
            d.SetActive(true);

            TermManager.UpdateTerm();
        }

        public GameObject GetDuplicateFromRoot()
        {
            var d = Instantiate(TermManager.FindRootTerm());
            d.name = d.name.Replace("(Clone)", "");
            d.SetActive(false);

            return d;
        }
    }
}
