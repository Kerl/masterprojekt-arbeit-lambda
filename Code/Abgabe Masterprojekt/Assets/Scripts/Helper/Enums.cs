﻿namespace Assets.Scripts.Helper
{
    public enum VariableColorIndices
    {
        Green,
        Blue,
        Red,
        Yellow,
        Grey
    };

    public enum TermTypes
    {
        Application,
        Abstraction,
        Variable,
        Clipboard,
        TypeInformation,
        Placeholder
    };
}