﻿using UnityEngine;

namespace Assets.Scripts.Helper
{
    public class Solver : MonoBehaviour
    {
        // Use this for initialization
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
        }

        public void Solve(GameObject input, int colorIndex)
        {
            var spawn = transform.Find("Visuals").Find("LeftSpawn").GetComponent<SpawnBehavior>();

            Insert(input, colorIndex);

            var currentGameObject = spawn.CurrentGameObject;
            currentGameObject.transform.position = transform.parent.position;
            if (transform.parent.parent != null)
            {
                currentGameObject.transform.SetParent(transform.parent.parent);
            }
            currentGameObject.transform.localScale = transform.parent.localScale;

            var p = transform.parent.parent;
            if (p != null && p.name != "Level")
            {
                var targetSpawn = p.Find("Visuals").Find("LeftSpawn").GetComponent<SpawnBehavior>();
                targetSpawn.CurrentGameObject = currentGameObject;
                targetSpawn.CurrentColorIndex =
                    transform.parent.Find("Visuals").Find("LeftSpawn").GetComponent<SpawnBehavior>().CurrentColorIndex;
            }

            Destroy(transform.parent.gameObject);
        }

        public void Insert(GameObject input, int colorIndex)
        {
            if (name == "Abstraction")
            {
                var spawn = transform.Find("Visuals").Find("LeftSpawn").GetComponent<SpawnBehavior>();

                PerformInsert(input, colorIndex, spawn);
            }
            else if (name == "Application")
            {
                var leftSpawn = transform.Find("Visuals").Find("LeftSpawn").GetComponent<SpawnBehavior>();
                var rightSpawn = transform.Find("Visuals").Find("RightSpawn").GetComponent<SpawnBehavior>();

                PerformInsert(input, colorIndex, leftSpawn);
                PerformInsert(input, colorIndex, rightSpawn);
            }
        }

        private void PerformInsert(GameObject input, int colorIndex, SpawnBehavior spawn)
        {
            if (colorIndex == spawn.CurrentColorIndex && !(spawn.CurrentColorIndex < 0))
            {
                var duplicate = Instantiate(input);
                duplicate.name = input.name;
                duplicate.transform.position = spawn.CurrentGameObject.transform.position;
                duplicate.transform.localScale = spawn.CurrentGameObject.transform.lossyScale;
                Destroy(duplicate.GetComponent<Rigidbody2D>());
                duplicate.transform.SetParent(gameObject.transform);

                var layerDiff =
                    spawn.CurrentGameObject.transform.Find("Visuals")
                        .Find("Background")
                        .GetComponent<SpriteRenderer>()
                        .sortingOrder -
                    input.transform.Find("Visuals").Find("Background").GetComponent<SpriteRenderer>().sortingOrder;

                LayerManager.ApplyLayerOffset(duplicate.transform, layerDiff);

                Destroy(spawn.CurrentGameObject);
                spawn.CurrentGameObject = duplicate;
            }
            else if (spawn.CurrentColorIndex < 0)
            {
                spawn.CurrentGameObject.GetComponent<Solver>().Insert(input, colorIndex);
            }
        }
    }
}