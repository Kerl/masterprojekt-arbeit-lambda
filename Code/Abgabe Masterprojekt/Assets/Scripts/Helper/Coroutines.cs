﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Helper
{
    public class Coroutines
    {
        public static IEnumerator CR_DoScale(Vector3 start, Vector3 end, float totalTime, GameObject gameObject, ICoroutineCompletionJob completionJob)
        {
            float t = 0;
            do
            {
                gameObject.transform.localScale = Vector3.Lerp(start, end, t/totalTime);
                yield return null;
                t += Time.deltaTime;

            } while (t < totalTime);

            gameObject.transform.localScale = end;

            if (completionJob != null)
            {
                completionJob.PerformJob(start, end, totalTime, gameObject);
            }
            yield break;
        }
    }
}