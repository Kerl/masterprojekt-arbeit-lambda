﻿using Assets.Scripts.Utility;
using UnityEngine;

namespace Assets.Scripts.Helper
{
    public static class LayerManager
    {
        private static readonly Color ApplicationColorOne = new Color(223f/255f, 222f/255f, 169f/255f);
        private static readonly Color ApplicationColorTwo = new Color(223f/255f, 169f/255f, 169f/255f);

        public static void ApplyLayerOffset(Transform ob, int offset)
        {
            SpriteRenderer sprite = ob.gameObject.GetComponent<SpriteRenderer>();

            if (sprite != null)
            {
                sprite.sortingOrder += offset;
            }

            foreach (Transform child in ob.transform)
            {
                var termComp = child.GetComponent<TermBehavior>();
                if (termComp != null && (termComp.TermType == TermTypes.Abstraction || termComp.TermType == TermTypes.Application || termComp.TermType == TermTypes.Variable || termComp.TermType == TermTypes.TypeInformation))
                {
                    ApplyLayerOffset(child, offset + 4);
                }
                else
                {
                    ApplyLayerOffset(child, offset);
                }
            }
        }

        public static void ApplyColor(GameObject ob)
        {
            if (!ob.name.Contains("Variable"))
            {
                //if (ob.transform.Find("Visuals") != null)
                if (ob.GetComponent<TermBehavior>() != null && !ob.GetComponent<TermBehavior>().HasStaticColor)
                {
                    var spriteRenderer = ob.transform.Find("Visuals").Find("Background").GetComponent<SpriteRenderer>();

                    if (spriteRenderer.sortingOrder%8 == 0)
                    {
                        spriteRenderer.color = ApplicationColorOne;
                    }
                    else
                    {
                        spriteRenderer.color = ApplicationColorTwo;
                    }
                }
            }


            foreach (Transform child in ob.transform)
            {
                ApplyColor(child.gameObject);
            }
        }

        public static void Flatten(GameObject ob)
        {
            SpriteRenderer sprite = ob.gameObject.GetComponent<SpriteRenderer>();
            if (sprite != null)
            {
                sprite.sortingOrder = sprite.sortingOrder%4;
            }

            foreach (Transform child in ob.transform)
            {
                Flatten(child.gameObject);
            }
        }

        public static void PerformLayerCleanup()
        {
            var root = GameObject.Find("Level").transform.GetChild(0);

            Flatten(root.gameObject);
            ApplyLayerOffset(root, 0);
            ApplyColor(root.gameObject);
        }
    }
}
