﻿using UnityEngine;

namespace Assets.Scripts.Helper
{
    public interface ICoroutineCompletionJob
    {
        void PerformJob(Vector3 start, Vector3 end, float totalTime, GameObject gameObject);
    }

    public class CompletionJobDisable : ICoroutineCompletionJob
    {
        public void PerformJob(Vector3 start, Vector3 end, float totalTime, GameObject gameObject)
        {
            //if (end == Vector3.zero)
            //{
            //    gameObject.SetActive(false);
            //}

            gameObject.SetActive(false);
        }
    }

    public class CompletionJobDelete : ICoroutineCompletionJob
    {
        public void PerformJob(Vector3 start, Vector3 end, float totalTime, GameObject gameObject)
        {
            if (end == Vector3.zero)
            {
                GameObject.Destroy(gameObject);
            }
        }
    }
}
