﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Assets.Scripts.Terms;
using Assets.Scripts.TypeSystem;
using Assets.Scripts.Utility;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.Helper
{
    public static class TermManager
    {
        public static void UpdateTerm()
        {
            LocateAbstractions(FindRootTerm());

            var solver = GameObject.Find("Utility").GetComponent<TermSolver>();
            if (solver == null)
            {
                throw new MissingComponentException("TermSolver");
            }

            if (solver.ApplicationToEval != null)
            {
                solver.ApplicationToEval.GetComponent<ApplicationBehavior>().SetPulsing(false);
            }

            solver.ApplicationToEval = FindNextEval(GameObject.Find("Level").transform.GetChild(0).gameObject);

            if (solver.ApplicationToEval != null)
            {
                solver.ApplicationToEval.GetComponent<ApplicationBehavior>().SetPulsing(true);
            }
        }

        private static void LocateAbstractions(GameObject ob)
        {
            foreach (Transform child in ob.transform)
            {
                LocateAbstractions(child.gameObject);
            }

            if (IsOfType(ob.gameObject, TermTypes.Abstraction))
            {
                ob.GetComponent<AbstractionBehavior>().BoundVariables.Clear();

                LocateVariables(ob, ob);
            }
        }

        private static void LocateVariables(GameObject abstraction, GameObject ob)
        {
            if (abstraction != ob && IsOfType(ob, TermTypes.Abstraction))
            {
                var currentTerm =
                    ob.transform.Find("Visuals").Find("HeadSpawn").GetComponent<HeadBehavior>().CurrentVariableId;

                if (currentTerm != null &&
                    currentTerm ==
                    abstraction.transform.Find("Visuals").Find("HeadSpawn").GetComponent<HeadBehavior>().CurrentVariableId)
                {
                    return;
                }
            }

            if (IsOfType(ob, TermTypes.Variable))
            {
                var currentTerm =
                    abstraction.transform.Find("Visuals").Find("HeadSpawn").GetComponent<HeadBehavior>().CurrentVariableId;

                var c = ob.GetComponent<VariableBehavior>();

                if (currentTerm == ob.GetComponent<VariableBehavior>().Id)
                {
                    ob.GetComponent<VariableBehavior>().BindingAbstraction =
                        abstraction.GetComponent<AbstractionBehavior>();
                    abstraction.GetComponent<AbstractionBehavior>().BoundVariables.Add(ob);
                }
            }

            foreach (Transform child in ob.transform)
            {
                LocateVariables(abstraction, child.gameObject);
            }
        }

        public static bool IsOfType(GameObject ob, TermTypes type)
        {
            if (ob.GetComponent<TermBehavior>() != null)
            {
                if (ob.GetComponent<TermBehavior>().TermType == type)
                {
                    return true;
                }
            }

            return false;
        }

        public static void PerformCleanup()
        {
            var r = SceneManager.GetActiveScene().GetRootGameObjects();

            for (int i = 0; i < r.Length; i++)
            {
                if (r[i].GetComponent<TermBehavior>() != null)
                {
                    GameObject.Destroy(r[i]);
                }
            }
        }

        public static List<int> FindParentAbstractions(GameObject spawn)
        {
            var variableList = new List<int>();

            var parent = spawn.transform;
            while (parent != null)
            {
                if (IsOfType(parent.gameObject, TermTypes.Abstraction))
                {
                    variableList.Add(parent.GetComponent<AbstractionBehavior>().GetCurrentVariableId());
                }

                parent = parent.transform.parent;
            }

            return variableList;
        }

        public static GameObject FindNextEval(GameObject ob)
        {
            if (ob == null || ob.GetComponent<TermBehavior>().TermType != TermTypes.Application)
            {
                return null;
            }
            else
            {
                var application = ob.GetComponent<ApplicationBehavior>();

                if (application.IsSolvable())
                {
                    var leftTermSolvable = FindNextEval(application.GetLeftTerm());

                    if (leftTermSolvable != null)
                    {
                        return leftTermSolvable;
                    }
                    else
                    {
                        return ob;
                    }
                }

                var rightTermSolvable = FindNextEval(application.GetRightTerm());

                if (rightTermSolvable != null)
                {
                    return rightTermSolvable;
                }
                else
                {
                    return FindNextEval(application.GetLeftTerm());
                }
            }

            return null;
        }

        public static GameObject FindRootTerm()
        {
            return GameObject.Find("Level").transform.GetChild(0).gameObject;
        }

        private static bool CheckFillablesForTree(GameObject ob)
        {
            if (!CheckFillablesForSingeTerm(ob)) return false;

            foreach (Transform child in ob.transform)
            {
                var check = CheckFillablesForTree(child.gameObject);

                if (!check)
                    return false;
            }

            return true;
        }

        private static bool CheckFillablesForSingeTerm(GameObject ob)
        {
            if (ob.GetComponent<IFillabeTerm>() != null)
            {
                var check = ob.GetComponent<IFillabeTerm>().CheckForFilledSlots();

                if (!check)
                    return false;
            }
            return true;
        }

        private static void SpwanPlaceholdersForTree(GameObject ob)
        {
            SpawnPlaceholdersForSingleTerm(ob);

            foreach (Transform child in ob.transform)
            {
                SpwanPlaceholdersForTree(child.gameObject);
            }
        }

        private static void SpawnPlaceholdersForSingleTerm(GameObject ob)
        {
            if (ob.GetComponent<IFillabeTerm>() != null)
            {
                ob.GetComponent<IFillabeTerm>().SpawnPlaceholders();
            }
        }

        public static bool CheckTreeForErrors()
        {
            var ret = CheckFillablesForTree(FindRootTerm());

            if (!ret)
            {
                SpwanPlaceholdersForTree(FindRootTerm());

                FindRootTerm().GetComponent<Animator>().SetTrigger("shake");
            }


            return ret;
        }

        public static bool CheckSingleTermForErrors(GameObject term)
        {
            var ret = CheckFillablesForSingeTerm(term);

            if (!ret)
            {
                SpawnPlaceholdersForSingleTerm(term);

                FindRootTerm().GetComponent<Animator>().SetTrigger("shake");
            }


            return ret;
        }

        public static bool CheckTreeForTypes()
        {
            var ret = CheckTypes(FindRootTerm());

            if (!ret)
            {
                FindRootTerm().GetComponent<Animator>().SetTrigger("shake");
            }

            return ret;
        }

        private static bool CheckTypes(GameObject ob)
        {
            if (ob == null)
            {
                return true;
            }

            var typesCorrect = true;
            var application = ob.GetComponent<ApplicationBehavior>();
            var abstraction = ob.GetComponent<AbstractionBehavior>();

            if (application != null)
            {
                var lTypeCheck = CheckTypes(application.GetLeftTerm());
                var rTypeCheck = CheckTypes(application.GetRightTerm());

                if (application.GetLeftTerm() != null && application.GetLeftTerm().GetComponent<ITypedTerm>() != null 
                    && application.GetRightTerm() != null && application.GetRightTerm().GetComponent<ITypedTerm>() != null 
                    && application.GetRightTerm().GetComponent<ITypedTerm>().GetLambdaType().GetType() == typeof (TypeFunction))
                {
                    if (rTypeCheck && lTypeCheck)
                    {
                        var leftType =
                        application.GetLeftTerm().GetComponent<ITypedTerm>().GetLambdaType();
                        var tempType =
                            (TypeFunction)(application.GetRightTerm().GetComponent<ITypedTerm>().GetLambdaType());
                        var rightType = tempType.LType;

                        if (TypeBase.Compare(leftType, rightType))
                        {
                            Debug.Log("TypeCheck successful: " + leftType + " - " + rightType);
                            application.HideTypeWarning();
                        }
                        else
                        {
                            Debug.Log("TypeCheck failed: " + leftType + " - " + rightType);
                            application.ShowTypeWarning();
                            typesCorrect = false;
                        }
                    }
                }

                typesCorrect = typesCorrect && lTypeCheck;
                typesCorrect = typesCorrect && rTypeCheck;
            }

            if (abstraction != null)
            {
                typesCorrect = typesCorrect & CheckTypes(abstraction.GetTerm());
            }

            return typesCorrect;
        }

        public static void StripVisuals(GameObject ob)
        {
            if (ob.GetComponent<SpriteRenderer>() != null)
            {
                GameObject.Destroy(ob.GetComponent<SpriteRenderer>());
            }

            if (ob.transform.Find("UI") != null)
            {
                GameObject.Destroy(ob.transform.Find("UI").gameObject);
            }

            foreach (Transform child in ob.transform)
            {
                StripVisuals(child.gameObject);
            }
        }
    }
}
