﻿using UnityEngine;

namespace Assets.Scripts.Helper
{
    public static class ExtensionMethods
    {
        public static void SetTransparency(this UnityEngine.UI.Image image, float transparency)
        {
            if (image != null)
            {
                var color = image.color;
                color.a = transparency;
                image.color = color;
            }
        }
    }
}
