﻿using UnityEngine;

namespace Assets.Scripts.Helper
{
    class VariableColors
    {
        public static Color GetColor(int index)
        {
            switch (index)
            {
                case (int)VariableColorIndices.Blue:
                    return new Color(72 / 255f, 79 / 255f, 214 / 255f, 255 / 255f);
                case (int)VariableColorIndices.Green:
                    return new Color(8 / 255f, 189 / 255f, 62 / 255f, 255 / 255f);
                case (int)VariableColorIndices.Grey:
                    return new Color(137 / 255f, 137 / 255f, 137 / 255f, 255 / 255f);
                case (int)VariableColorIndices.Red:
                    return new Color(236 / 255f, 102 / 255f, 105 / 255f, 255 / 255f);
                case (int)VariableColorIndices.Yellow:
                    return new Color(164 / 255f, 182 / 255f, 0 / 255f, 255 / 255f);
            }

            return Color.magenta;
        }
    }
}